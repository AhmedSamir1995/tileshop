﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesPool : MonoBehaviour
{
    public GameObject tilePrefab;
    public List<GameObject> tiles;

    public int initialCount;

    public static TilesPool instance;

    private void Awake()
    {
        if(instance)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateNTilesInPool(initialCount);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetTile()
    {
        if(tiles.Count>0)
        {
            GameObject temp = tiles[0];
            tiles.RemoveAt(0);
            temp.SetActive(true);
            temp.transform.SetParent(null);
            return temp;
        }
        GenerateNTilesInPool(initialCount);

        return GameObject.Instantiate(tilePrefab);
    }

    public void GenerateNTilesInPool(int n)
    {
        GameObject temp;

        for(int i=0;i<n;i++)
        {
            temp = GameObject.Instantiate(tilePrefab);
            AddTileToPool(temp);
        }
    }

    public void AddTileToPool(GameObject tile)
    {
        tile.SetActive(false);
        tile.transform.SetParent(transform);
        tiles.Add(tile);
    }
}
