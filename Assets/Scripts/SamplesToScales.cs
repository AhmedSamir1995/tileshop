﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamplesToScales : MonoBehaviour
{
    public GameObject cubePrefab;
    public List<GameObject> cubes;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void setScales(float[] scales)
    {
        if (scales.Length > cubes.Count)
            GenerateN(scales.Length - cubes.Count);
        for (int i = 0; i < scales.Length; i++)
        {
            cubes[i].transform.localScale = new Vector3(1, scales[i]*10, 1);
        }
    }
    public void GenerateN(int n)
    {
        for (int i = 0; i < n; i++)
        {
            GameObject temp = Instantiate(cubePrefab);
            temp.transform.position = new Vector3(1.5f*i, 0, 0);
            cubes.Add(temp);
        }
    }
}
