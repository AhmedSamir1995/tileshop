﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float playerSpeed;

    public GameObject tilePrefab;
    public GameObject tilesContainer;


    public Vector2 minMaxTilePosX;
    float lastTilePosition=0;
    public int initialTilesCount;
    public static GameManager instance;

    public float playTime;
    public bool isGameRunning;

    public UnityEngine.UI.Text afterGameText;
    public UnityEngine.UI.Text inGameText;

    public UnityEngine.UI.Button.ButtonClickedEvent onStartGame;
    public UnityEngine.UI.Button.ButtonClickedEvent onEndGame;
    public UnityEngine.UI.Button.ButtonClickedEvent onPauseGame;
    // Start is called before the first frame update
    void Start()
    {
        if(instance)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(isGameRunning)
        {
            playTime += Time.deltaTime;
            inGameText.text = "Time: " + ((int)playTime).ToString();
        }
    }

    [ContextMenu("InitializeGame")]
    public void InitializeGame()
    {
        playTime = 0;
        playerSpeed = Player.instance.zSpeed;
        lastTilePosition = 0;
        TilesDestroyer.instance.CheckUnWantedTiles();
        InstantiateNTiles(initialTilesCount);
        Player.instance.Reset();
    }
    [ContextMenu("StartGame")]
    public void StartGame()
    {
        onStartGame.Invoke();
        InitializeGame();
        isGameRunning = true;
    }

    public void ResumeGame()
    {
        RunGame();
    }
    public void PauseGame()
    {
        StopGame();
        onPauseGame.Invoke();
    }
    public void StopGame()
    {
        isGameRunning = false;
    }
    public void RunGame()
    {
        isGameRunning = true;
    }
    [ContextMenu("EndGame")]
    public void EndGame()
    {
        StopGame();
        onEndGame.Invoke();
        afterGameText.text = "Congrats!\nYou survived: " + (int)playTime + " seconds";
    }

    public void InstantiateNTiles(int n)
    {
        for(int i=0;i<n;i++)
        {
            GenerateTile();
        }
    }

    public void GenerateTile()
    {
            lastTilePosition += Player.instance.interval * playerSpeed;
        GameObject temp = TilesPool.instance.GetTile();
        temp.transform.SetParent(tilesContainer.transform);
        temp.transform.position = new Vector3(Random.Range(minMaxTilePosX.x, minMaxTilePosX.y),
            -0.5f,
            lastTilePosition);
    }
}
