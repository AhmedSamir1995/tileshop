﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject player;
    Vector3 delta;
    // Start is called before the first frame update
    void Start()
    {
        delta = player.transform.position-transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = player.transform.position - delta;
        SetPosition(player.transform.position - delta);
    }
    public void SetPosition(Vector3 targetPosition)
    {
        Vector3 myPosition = transform.position;
        myPosition.z = targetPosition.z;
        transform.position = myPosition;
    }
}
