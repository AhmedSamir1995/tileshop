﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject player;
    public float height;
    public float interval;
    public float nextInterval;
    public float nextHopTime;
    public float passedTime;
    public float upTime;
    public float zSpeed;
    public AnimationCurve a;

    public static Player instance;
    private void Awake()
    {
        if (instance)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        passedTime = 0;
        transform.position = Vector3.zero;
        nextHopTime = Time.time;
    }
    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isGameRunning)
            return;
        CheckHoping();
        SetMyZ(transform.position.z + zSpeed * Time.deltaTime);
    }
    public void SetMyY(float y)
    {
        Vector3 position = transform.position;
        position.y = y;
        transform.position = position;
    }

    public void SetMyZ(float z)
    {
        Vector3 position = transform.position;
        position.z = z;
        transform.position = position;
    }

    public void CheckHoping()
    {

        passedTime += Time.deltaTime;
        if (passedTime > interval) //if Hop time has passed
        {
            interval = nextInterval;
            nextHopTime += interval;
            passedTime -= interval;
            TilesDestroyer.instance.CheckUnWantedTiles();
            //Check if we lose

            RaycastHit hit;
            if (!Physics.Raycast(transform.position, -transform.up, out hit))
            {
                GameManager.instance.EndGame();
            }

        }


        SetMyY(a.Evaluate(passedTime / interval) * height);
    }

    public void SetNextInterval(float i)
    {
        nextInterval = i;
    }

    public void SetSpeed(float speed)
    {
        zSpeed = speed;
    }
}
