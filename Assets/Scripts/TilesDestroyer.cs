﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesDestroyer : MonoBehaviour
{
    public new GameObject camera;
    public GameObject tilesContainer;
    public bool endless;

    public static TilesDestroyer instance;

    private void Awake()
    {
        if (instance)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [ContextMenu("CheckUnWantedTiles")]
    public void CheckUnWantedTiles()
    {
        print(tilesContainer.transform.childCount);
        for(int i=0;i<tilesContainer.transform.childCount;i++)
        {
            GameObject temp = tilesContainer.transform.GetChild(i).gameObject;
            if (temp.transform.position.z<camera.transform.position.z || !GameManager.instance.isGameRunning)
            {
                print("Removing tiles");
                TilesPool.instance.AddTileToPool(temp);
                if(endless&&GameManager.instance.isGameRunning)
                    GameManager.instance.GenerateTile();
                i -= 1;
            }
        }
    }
}
