﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioToGraph : MonoBehaviour
{
    float[] samples;
    public AnimationCurve curve;
    public new AudioClip audio;
    public int offset;
    public AudioDataLoadState state;
    public int samplesN;
    public AudioSource audioSource;
    public GameObject cube;
    public float sampleRate;
    public float threshold;
    public float peak;
    // Start is called before the first frame update
    void Start()
    {
        sampleRate = audio.frequency;
        pastNSamples = new Stack<float>();
        audio.LoadAudioData();
        samplesN = audio.samples;
        state = audio.loadState;
        samples = new float[audio.samples*audio.channels];
        audio.GetData(samples, offset);
    }
    float lastSize;
    // Update is called once per frame
    void Update()
    {
        float sampleSize=samples[audioSource.timeSamples];
        if (sampleSize > peak)
            peak = sampleSize;
        AddSample(sampleSize);
        float avg = GetAVG();
        if(avg >threshold)
        { print(avg); 
        Vector3 scale = cube.transform.localScale;
        scale.y = avg;
        cube.transform.localScale = scale;
        }
        //audioSource.GetOutputData(audioSource.timeSamples,out sampleSize);
        lastSize = sampleSize;
    }
    Stack<float> pastNSamples;
    float GetAVG()
    {
        float[] samples = pastNSamples.ToArray();
        float avg=0;
        for(int i=0;i<samples.Length;i++)
        {
            avg += samples[i];
        }
        print("Samples length:" + samples.Length);
        return avg / samples.Length;
    }
    void AddSample(float S)
    {
        if (pastNSamples.Count > 1)
            pastNSamples.Pop();
        pastNSamples.Push(S);
    }
}
