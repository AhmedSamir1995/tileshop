﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject player;
    public float sensitivity;
    Vector3 mouseInitialPosition;

    private void Update()
    {
        if (!GameManager.instance.isGameRunning)
            return;
        if(Input.GetMouseButtonDown(0))
        {
            mouseInitialPosition = Input.mousePosition;
        }
        if(Input.GetMouseButton(0))
        {
            Vector3 playerPos= player.transform.position;
            playerPos.x += (Input.mousePosition - mouseInitialPosition).x*sensitivity;
            player.transform.position = playerPos;
            mouseInitialPosition = Input.mousePosition;
        }
    }
}
